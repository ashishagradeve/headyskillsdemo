//
//  ReachibilityOperation.swift
//  SmallCase
//
//  Created by Ashish Agrawal on 27/01/19.
//  Copyright © 2019 Ashish Agrawal. All rights reserved.
//

import UIKit
import Reachability

class NetworkReachable {
    
    //MARK:- Static Singleton Varaiable
    static let shared = NetworkReachable()
    //MARK:- Contant
    let reachibilityConnection = NSNotification.Name(rawValue: "reachibilityConnection")
    //MARK:- Lazy Varaiable
    lazy var reachability = Reachability.init()!
    
    //MARK:- init
    init () {
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    //MARK:- Methods
    
    func isInternetAvailableReachibilty() -> Bool {
        switch reachability.connection {
        case .cellular:
            NotificationCenter.default.post(name: reachibilityConnection, object: nil, userInfo: ["connect":"Reachable via Cellular"])
            return true
        case .wifi:
            NotificationCenter.default.post(name: reachibilityConnection, object: nil, userInfo: ["connect":"Reachable via Cellular"])
            return true
        case .none:
            //        showToast(message: "Please turn on mobile data or connect to WiFi".localized())
            return false
        }
    }
    
    func checkInternetReachibilityBlock(reachable:@escaping (_ reachable:Bool) -> Void) {
        reachability.whenReachable = { reachability in
            // this is called on a background thread, but UI updates must
            // be on the main thread, like this:
            DispatchQueue.main.async {
                if reachability.connection == .wifi || reachability.connection == .cellular{
                    reachable(true)
                } else {
                    reachable(false)
                }
            }
        }
        
        reachability.whenUnreachable = { reachability in
            // this is called on a background thread, but UI updates must
            // be on the main thread, like this:
            DispatchQueue.main.async {
                if reachability.connection == .wifi || reachability.connection == .cellular{
                    reachable(true)
                } else {
                    reachable(false)
                }
            }
        }
    }
}






