//
//  ReuseableProtocol.swift
//  SmallCase
//
//  Created by Ashish Agrawal on 27/01/19.
//  Copyright © 2019 Ashish Agrawal. All rights reserved.
//

import UIKit

protocol ReuseableProtocol {
    static var reusableIdentifier:String {
        get
    }
}

extension ReuseableProtocol {
    static var reusableIdentifier:String {
        get {
            return "\(self)"
        }
    }
}
