//
//  NetworkImageDownload.swift
//  SmallCase
//
//  Created by Ashish Agrawal on 27/01/19.
//  Copyright © 2019 Ashish Agrawal. All rights reserved.
//

import UIKit

class NetworkImageDownload: Operation {
  
    func getDownloadImageUrl(url:String? , image: @escaping (_ image:UIImage?) -> Void) {
        getImageWith(url) {(data, error) in
            if let data = data {
                image(UIImage.init(data: data))
            } else {
                image(nil)
            }
        }
    }
    
    func getImageWith(_ imgUrl: String?, handler: @escaping ((_ data: Data?, NSError?) -> Void))
    {
        if let imgUrl = imgUrl?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) , imgUrl.characters.count > 0 , let url  = URL.init(string: imgUrl) {
            let fileName = imgUrl.split(separator: "/").last
            
            let tempDirectory = URL(fileURLWithPath: NSTemporaryDirectory(), isDirectory: true)
            let finalPath = tempDirectory.appendingPathComponent(String(fileName!))
            
            let fileExists = FileManager.default.fileExists(atPath: finalPath.path)
            if (fileExists) {
                if let imgData = try? Data(contentsOf: URL(fileURLWithPath: finalPath.path)) {
                    handler(imgData, nil)
                }
                return
            } else {
                
                // if not, download image from url
                URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                    if let error = error {
                        print(error.localizedDescription)
                        handler(nil, error as NSError)
                        return
                    }
                    if let imgData = data , let _ = UIImage.init(data: imgData) {
                        DispatchQueue.main.async {
                            do {
                                try imgData.write(to: finalPath)
                                handler(imgData, nil)
                            } catch {
                                print(error)
                            }
                        }
                    } else {
                        handler(nil, nil)
                    }
                }).resume()
            }
        }
    }
}
