//
//  ViewController.swift
//  SmallCase
//
//  Created by Ashish Agrawal on 22/01/19.
//  Copyright © 2019 Ashish Agrawal. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {

    var viewModel = HomeViewModel()
    
    // MARK: - View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
}

// MARK: - UICollectionViewDataSource
extension HomeVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeCVCell.reusableIdentifier, for: indexPath) as! HomeCVCell
        
        return cell
    }
}
